package com.amdocs;

import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {
    public IncrementTest(){};
    @Test
    public void testAdd() throws Exception {
	Increment inc = new Increment();
        inc.decreasecounter(1);
	final int count = inc.getCounter();
        assertEquals("Count", 1, count);

    }
    @Test
    public void testCount() throws Exception {
	Increment inc = new Increment();
        final  int count= inc.decreasecounter(0);
        assertEquals("Count", 1, count);

    }

    @Test
    public void testCount2() throws Exception {
	Increment inc = new Increment();
        final int count= inc.decreasecounter(2);
        assertEquals("Count", 1, count);

    }
}

